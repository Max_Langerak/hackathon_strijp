echo "removing old db"
rm db/development.sqlite3
rm db/test.sqlite3

echo "Generating new db"
rake db:create

echo "migrating"
rake db:migrate

echo "Generating new ERD file"
rake erd

echo "Seeding"
rake db:seed

echo "Done"
