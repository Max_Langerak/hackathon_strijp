# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171007032337) do

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.integer "municipality_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deadlines", force: :cascade do |t|
    t.datetime "when"
    t.string "what"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "forumposts", force: :cascade do |t|
    t.text "content"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "forumtopic_id"
  end

  create_table "forumtopics", force: :cascade do |t|
    t.string "name"
    t.integer "project_id"
    t.integer "user_id"
    t.integer "rights"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "licenserequirements", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "license_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "licenses", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "municipalities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "municipalityusers", force: :cascade do |t|
    t.integer "user_id"
    t.integer "municipality_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projectfiles", force: :cascade do |t|
    t.string "name"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projectlicenses", force: :cascade do |t|
    t.integer "project_id"
    t.integer "license_id"
    t.integer "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projectlocations", force: :cascade do |t|
    t.integer "project_id"
    t.integer "city_id"
    t.string "address"
    t.string "zipcode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projectrequirements", force: :cascade do |t|
    t.string "name"
    t.integer "state"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "projecttype_id"
    t.integer "state"
  end

  create_table "projecttags", force: :cascade do |t|
    t.integer "tag_id"
    t.integer "project_id"
    t.integer "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projecttypelicenses", force: :cascade do |t|
    t.integer "license_id"
    t.integer "projecttype_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projecttyperequirements", force: :cascade do |t|
    t.string "name"
    t.integer "projecttype_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projecttypes", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projecttypetags", force: :cascade do |t|
    t.integer "projecttype_id"
    t.integer "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projectusers", force: :cascade do |t|
    t.integer "user_id"
    t.integer "project_id"
    t.integer "projectusertype_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projectusertags", force: :cascade do |t|
    t.integer "projecttag_id"
    t.integer "projectuser_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projectusertypes", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "usertags", force: :cascade do |t|
    t.integer "user_id"
    t.integer "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
