class CreateProjecttyperequirements < ActiveRecord::Migration[5.1]
  def change
    create_table :projecttyperequirements do |t|
      t.string :name
      t.integer :projecttype_id

      t.timestamps
    end
  end
end
