class CreateProjecttags < ActiveRecord::Migration[5.1]
  def change
    create_table :projecttags do |t|
      t.integer :tag_id
      t.integer :project_id
      t.integer :state

      t.timestamps
    end
  end
end
