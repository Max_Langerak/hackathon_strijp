class AddProjecttypeIdToProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :projecttype_id, :integer
  end
end
