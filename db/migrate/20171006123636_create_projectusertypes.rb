class CreateProjectusertypes < ActiveRecord::Migration[5.1]
  def change
    create_table :projectusertypes do |t|
      t.string :name

      t.timestamps
    end
  end
end
