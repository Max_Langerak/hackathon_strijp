class CreateLicenserequirements < ActiveRecord::Migration[5.1]
  def change
    create_table :licenserequirements do |t|
      t.string :name
      t.text :description
      t.integer :license_id

      t.timestamps
    end
  end
end
