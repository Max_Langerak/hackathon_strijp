class CreateProjecttypelicenses < ActiveRecord::Migration[5.1]
  def change
    create_table :projecttypelicenses do |t|
      t.integer :license_id
      t.integer :projecttype_id

      t.timestamps
    end
  end
end
