class AddStateToProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :state, :integer
  end
end
