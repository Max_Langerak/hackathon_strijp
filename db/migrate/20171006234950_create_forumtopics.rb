class CreateForumtopics < ActiveRecord::Migration[5.1]
  def change
    create_table :forumtopics do |t|
      t.string :name
      t.integer :project_id
      t.integer :user_id
      t.integer :rights

      t.timestamps
    end
  end
end
