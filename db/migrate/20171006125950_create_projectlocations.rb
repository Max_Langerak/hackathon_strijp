class CreateProjectlocations < ActiveRecord::Migration[5.1]
  def change
    create_table :projectlocations do |t|
      t.integer :project_id
      t.integer :city_id
      t.string :address
      t.string :zipcode

      t.timestamps
    end
  end
end
