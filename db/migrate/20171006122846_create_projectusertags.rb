class CreateProjectusertags < ActiveRecord::Migration[5.1]
  def change
    create_table :projectusertags do |t|
      t.integer :projecttag_id
      t.integer :projectuser_id

      t.timestamps
    end
  end
end
