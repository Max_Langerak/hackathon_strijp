class CreateProjecttypetags < ActiveRecord::Migration[5.1]
  def change
    create_table :projecttypetags do |t|
      t.integer :projecttype_id
      t.integer :tag_id

      t.timestamps
    end
  end
end
