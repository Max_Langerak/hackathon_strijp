class AddForumtopicIdToForumpost < ActiveRecord::Migration[5.1]
  def change
    add_column :forumposts, :forumtopic_id, :integer
  end
end
