class CreateMunicipalityusers < ActiveRecord::Migration[5.1]
  def change
    create_table :municipalityusers do |t|
      t.integer :user_id
      t.integer :municipality_id

      t.timestamps
    end
  end
end
