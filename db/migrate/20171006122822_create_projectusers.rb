class CreateProjectusers < ActiveRecord::Migration[5.1]
  def change
    create_table :projectusers do |t|
      t.integer :user_id
      t.integer :project_id
      t.integer :projectusertype_id

      t.timestamps
    end
  end
end
