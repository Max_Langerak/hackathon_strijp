class CreateForumposts < ActiveRecord::Migration[5.1]
  def change
    create_table :forumposts do |t|
      t.text :content
      t.integer :user_id

      t.timestamps
    end
  end
end
