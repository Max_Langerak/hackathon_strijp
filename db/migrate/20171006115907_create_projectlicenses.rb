class CreateProjectlicenses < ActiveRecord::Migration[5.1]
  def change
    create_table :projectlicenses do |t|
      t.integer :project_id
      t.integer :license_id
      t.integer :state

      t.timestamps
    end
  end
end
