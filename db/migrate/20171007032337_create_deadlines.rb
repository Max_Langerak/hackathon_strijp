class CreateDeadlines < ActiveRecord::Migration[5.1]
  def change
    create_table :deadlines do |t|
      t.datetime :when
      t.string :what
      t.integer :project_id

      t.timestamps
    end
  end
end
