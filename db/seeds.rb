# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Projectusertype.create(name: "Owner")
Projectusertype.create(name: "Employee")
Projectusertype.create(name: "Neighbour")

Projecttype.create(name: "verbouwing")
Projecttype.create(name: "Restaurant openen")
Projecttype.create(name: "Nieuw ding bouwen")

Municipality.create(name: "Eindhoven")
Municipality.create(name: "Helmond")

City.create(name: "Eindhoven", municipality_id: 1)
City.create(name: "Helmond", municipality_id: 2)

Projecttyperequirement.create(name: "Moet binnen 50x50 blijven", projecttype_id: 1)
Projecttyperequirement.create(name: "Binnenhuisarchitect moet geregeld worden", projecttype_id: 1)

License.create(name: "Bouwlicentie", description:"Je moet fixen enzo")
Licenserequirement.create(name: "Binnen bestemmingsplan", description: "Moet binnen bestemmingsplan blijven", license_id: 1)

Projecttypelicense.create(license_id: 1, projecttype_id: 1)

Tag.create(name: "aannemer")
Tag.create(name: "ambtenaar")
Tag.create(name: "architect")
Tag.create(name: "andere dingen")
Projecttypetag.create(tag_id: 1, projecttype_id: 1)
Projecttypetag.create(tag_id: 2, projecttype_id: 1)
Projecttypetag.create(tag_id: 3, projecttype_id: 1)