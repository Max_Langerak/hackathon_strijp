class Municipality < ApplicationRecord

    has_many :cities
    has_many :municipalityusers

end
