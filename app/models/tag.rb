class Tag < ApplicationRecord

    has_many :usertags
    has_many :projecttags
    has_many :projecttypetags

end
