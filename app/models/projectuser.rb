class Projectuser < ApplicationRecord

    belongs_to :project
    belongs_to :user
    belongs_to :projectusertype

    has_many :projectusertags

end
