class City < ApplicationRecord

    has_many :projectlocations
    belongs_to :municipality

end
