class License < ApplicationRecord

    has_many :licenserequirements
    has_many :projectlicenses
    has_many :projecttypelicenses

end
