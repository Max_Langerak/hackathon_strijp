class Projecttype < ApplicationRecord

    has_many :projects
    has_many :projecttypelicenses
    has_many :projecttyperequirements
    has_many :projecttypetags

end
