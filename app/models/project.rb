class Project < ApplicationRecord

    has_many :projectrequirements
    has_many :projectlicenses
    has_many :projecttags
    has_many :projectusers
    has_many :forumtopics
    has_many :projectfiles
    has_many :deadlines
    has_one  :projectlocation

    belongs_to :projecttype
end
