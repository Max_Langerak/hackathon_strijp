class ForumtopicsController < ApplicationController

	before_action :authenticate_user!
	before_action :find_forumtopic, only: [:show, :edit, :update, :destroy]

	def index
		@forumtopics ||= []

		Forumtopic.all.each do |forumtopic|
			current_user.projectusers.all.each do |pj|
				if pj.project_id == forumtopic.project_id
					if rights >= pj.projectusertype_id
						@forumtopics << forumtopic
					end
				end
			end
		end
	end

	def show
		# make sure the user is allowed to see this
		is_allowed = false

		current_user.projectusers.all.each do |pj|
			if p.project_id == @forumtopic.project_id
				if pj.projectusertype_id <= rights
					is_allowed = true
					break
				end
			end
		end

		unless is_allowed
			redirect_to root_path
		else
			# just for the idea we'll create the
			# forumpost in the controller
			@forumpost = @groupforumtopic.forumposts.build
			@forumpost.user_id = current_user.id
		end
	end

	def new
	end

	def create
	end

	def edit
	end

	def update
	end

	def destroy
	end

	private

	def forumtopic_params
		params.require(:forumtopic).permit([:name, :user_id, :project_id, :rights])
	end

	def find_forumtopic
		@forumtopic = Forumtopic.find_by_id(params[:id])
	end

end
