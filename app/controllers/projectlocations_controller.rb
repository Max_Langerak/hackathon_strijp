class ProjectlocationsController < ApplicationController

	before_action :authenticate_user!

	def new
		@project = Project.find_by_id(params[:project_id])
		is_owner = false

        @project.projectusers.all.each do |projectuser|
            if projectuser.user_id == current_user.id
                if projectuser.projectusertype_id == 1
                    is_owner = true
                end
                break
            end
        end

        if is_owner
			@projectlocation = Projectlocation.new
		else
			redirect_to root_path
		end
	end

	def create
		@projectlocation = Projectlocation.new(projectlocation_params)
		@project = Project.find_by_id(@projectlocation.project_id)

		is_owner = false
        @project.projectusers.all.each do |projectuser|
            if projectuser.user_id == current_user.id
                if projectuser.projectusertype_id == 1
                    is_owner = true
                end
                break
            end
        end

        if is_owner
        	if @projectlocation.save
                # generate the stuff from the projecttypes
                # make sure that we can do everything necessery

                @project.projecttype.projecttypelicenses.all.each do |license|
                    projectlicense = Projectlicense.new
                    projectlicense.state = 1
                    projectlicense.license_id = license.license_id
                    projectlicense.project_id = @project.id

                    projectlicense.save
                end

                @project.projecttype.projecttyperequirements.all.each do |requirement|
                    projectrequirement = Projectrequirement.new
                    projectrequirement.project_id = @project.id
                    projectrequirement.name = requirement.name
                    projectrequirement.state = 1

                    projectrequirement.save
                end

                @project.projecttype.projecttypetags.all.each do |tag|
                    projecttag = Projecttag.new
                    projecttag.tag_id = tag.tag_id
                    projecttag.project_id = @project.id
                    projecttag.state = 1

                    projecttag.save
                end

        		redirect_to @project
        	else
        		render 'new'
        	end
        else
        	redirect_to root_path
        end

	end

	private

	def projectlocation_params
		params.require(:projectlocation).permit([:project_id, :city_id, :address, :zipcode])
	end

end
