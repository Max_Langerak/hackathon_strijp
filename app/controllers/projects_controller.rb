class ProjectsController < ApplicationController

    before_action :authenticate_user!
    before_action :find_project, only: [:show, :edit, :update, :destroy]

    def index

        @own_projects ||= []
        @employee_projects ||= []
        @neighbouring_projects ||= []

        current_user.projectusers.all.each do |projectuser|
            if projectuser.projectusertype.id == 1
                @own_projects << projectuser.project
            end

            if projectuser.projectusertype.id == 2
                @employee_projects << projectuser.project
            end

            if projectuser.projectusertype.id == 3
                @own_projects << projectuser.project
            end
        end
    end

    def show
        # make sure the user is authenticated
        # we will now check whether or not the project has the correct shit

        if @project.state == nil
            # this probably means this hasn't been instantiated #fixthis
            @project.state = 1
            @project.save
        end

        @counter = 0

        if @project.state == 1

            @project.projectrequirements.all.each do |require|
                if require.state == 1
                    @counter += 1
                end
            end

            if @counter == 0
                @project.state = 2
                @project.save
            end
        end

        if @project.state == 2
            @project.projecttags.all.each do |tag|
                if tag.state == 1
                    @counter += 1
                end
            end

            if @counter == 0
                @project.state = 3
                @project.save
            end
        end

        if @project.state == 3
            @project.projectlicenses.all.each do |license|
                if license.state == 1
                    @counter += 1
                end

                if license.state == 3
                    @counter = -1
                    break
                end
            end

            if @counter == 0
                @project.state = 4
                @project.save
            end
        end

        is_authenticated = false

        @project.projectusers.all.each do |projectuser|
            if projectuser.user_id == current_user.id
                is_authenticated = true
                break
            end
        end

        if is_authenticated
            @projectuser = Projectuser.new 
            @projectuser.project_id = @project.id 
            @projectuser.projectusertype_id = 2

            # this way we can add more data
            # make sure we find all the correct data here
        else
            redirect_to root_path
        end
    end

    def new
        @project = Project.new
    end

    def create
        @project = Project.new( project_params )

        if @project.save
            # make sure we create a projectuser

            projectuser = Projectuser.new

            projectuser.user_id = current_user.id
            projectuser.project_id = @project.id
            projectuser.projectusertype_id = 1

            #already create the forum; just for fun
            forumtopic = Forumtopic.new
            forumtopic.name = "alleen voor eigenaren"
            forumtopic.rights = 1
            forumtopic.user_id = current_user.id
            forumtopic.save

            forumtopic = Forumtopic.new
            forumtopic.name = "Alles voor de development"
            forumtopic.rights = 2
            forumtopic.user_id = current_user.id
            forumtopic.save

            forumtopic = Forumtopic.new
            forumtopic.name = "Publiek forum"
            forumtopic.rights = 3
            forumtopic.user_id = current_user.id
            forumtopic.save

            projectuser.save

            redirect_to new_projectlocation_path( project_id: @project.id )
        else
            render 'new'
        end
    end

    def edit
        #check if this guy is an owner

        is_owner = false

        @project.projectuser.all.each do |projectuser|
            if projectuser.user_ud == current_user.id
                if projectuser.projectusertype_id == 1
                    is_owner = true
                end
                break
            end
        end

        unless is_owner
            redirect_to root_path
        end
    end

    def update
        is_owner = false

        @project.projectuser.all.each do |projectuser|
            if projectuser.user_ud == current_user.id
                if projectuser.projectusertype_id == 1
                    is_owner = true
                end
                break
            end
        end

        unless is_owner
            redirect_to root_path
        else

            if @project.update( project_params )
                redirect_to @project
            else
                render 'edit'
            end

        end
    end

    def destroy
        is_owner = false

        @project.projectuser.all.each do |projectuser|
            if projectuser.user_ud == current_user.id
                if projectuser.projectusertype_id == 1
                    is_owner = true
                end
                break
            end
        end

        if is_owner
            @project.destroy
            redirect_to root_path
        else
            redirect_to root_path
        end

    end

    private

    def find_project
    	@project = Project.find_by_id(params[:id])
    end

    def project_params
        params.require(:project).permit([:name, :projecttype_id])
    end

end
