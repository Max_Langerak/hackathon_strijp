class ProjectusersController < ApplicationController

	before_action :authenticate_user!

	def index
		if params[:email]
			users = User.where(:email => params[:email])

			if users.count > 0

				user = users.first	

				@user_id = user.id
				@exists = true

				respond_to do |format|
					format.json
				end

			else
				@exists = false
				@user_id = -1

				respond_to do |format|
					format.json
				end
			end
		end
	end

	def create
		@projectuser = Projectuser.new( projectuser_params )
		@projectuser.save

		#check all the tags
		@projectuser.user.usertags.all.each do |usertag|

			@projectuser.project.projecttags.all.each do |projecttag|
				if projecttag.tag_id == usertag.tag_id
					projecttag.state = 2
					projecttag.save
					projectusertag = Projectusertag.new
					projectusertag.projecttag_id = projecttag.id
					projectusertag.projectuser_id = usertag.user_id
					projectusertag.save
				end
			end
		end

		redirect_to @projectuser.project
	end

	private

	def projectuser_params
		params.require(:projectuser).permit(:project_id, :user_id, :projectusertype_id)
	end

end
