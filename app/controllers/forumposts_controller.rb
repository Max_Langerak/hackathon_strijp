class ForumpostsController < ApplicationController

	before_action :authenticate_user

	def create

		@forumpost = Forumpost.new(forumpost_params)

		if current_user.id == @forumpost.user_id
			@forumpost.save

			redirect_to @forumpost.forumtopic
		end

	end

	private

	def forumpost_params
		params.require(:forumpost).permit([:user_id, :content, :forumtopic_id])
	end

end
